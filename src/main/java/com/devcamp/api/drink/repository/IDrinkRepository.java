package com.devcamp.api.drink.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.drink.model.CDrink;


public interface IDrinkRepository extends JpaRepository<CDrink, Long> {

}
