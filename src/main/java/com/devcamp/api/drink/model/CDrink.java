package com.devcamp.api.drink.model;

import javax.persistence.*;

@Entity
@Table(name="drinks")
public class CDrink {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="ma_nuoc_uong")
	private String maNuocUong;
	
	@Column(name="ten_nuoc_uong")
	private String tenNuocUong;
	
	@Column(name="don_gia")
	private long donGia;
	
	@Column(name="ngay_tao")
	private long ngayTao;
	
	@Column(name="ngay_cap_nhat")
	private long ngayCapNhat;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public CDrink() {
	}

	public CDrink(long id, String maNuocUong, String tenNuocUong, long donGia, long ngayTao, long ngayCapNhat) {
		this.id = id;
		this.maNuocUong = maNuocUong;
		this.tenNuocUong = tenNuocUong;
		this.donGia = donGia;
		this.ngayTao = ngayTao;
		this.ngayCapNhat = ngayCapNhat;
	}

	/**
	 * @return the maNuocUong
	 */
	public String getMaNuocUong() {
		return maNuocUong;
	}

	/**
	 * @param maNuocUong the maNuocUong to set
	 */
	public void setMaNuocUong(String maNuocUong) {
		this.maNuocUong = maNuocUong;
	}

	/**
	 * @return the tenNuocUong
	 */
	public String getTenNuocUong() {
		return tenNuocUong;
	}

	/**
	 * @param tenNuocUong the tenNuocUong to set
	 */
	public void setTenNuocUong(String tenNuocUong) {
		this.tenNuocUong = tenNuocUong;
	}

	/**
	 * @return the donGia
	 */
	public long getDonGia() {
		return donGia;
	}

	/**
	 * @param donGia the donGia to set
	 */
	public void setDonGia(long donGia) {
		this.donGia = donGia;
	}

	/**
	 * @return the ngayTao
	 */
	public long getNgayTao() {
		return ngayTao;
	}

	/**
	 * @param ngayTao the ngayTao to set
	 */
	public void setNgayTao(long ngayTao) {
		this.ngayTao = ngayTao;
	}

	/**
	 * @return the ngayCapNhat
	 */
	public long getNgayCapNhat() {
		return ngayCapNhat;
	}

	/**
	 * @param ngayCapNhat the ngayCapNhat to set
	 */
	public void setNgayCapNhat(long ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}
}
