package com.devcamp.api.drink.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.drink.model.CDrink;
import com.devcamp.api.drink.repository.IDrinkRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CDrinkController {
	@Autowired
	IDrinkRepository pDrinkRepository;

	@GetMapping("/drinks")
	public ResponseEntity<List<CDrink>> getAllVoucher() {
		try {
			List<CDrink> pDrinks = new ArrayList<CDrink>();
			pDrinkRepository.findAll().forEach(pDrinks::add);
			return new ResponseEntity<>(pDrinks, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
